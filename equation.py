import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.interpolate import make_interp_spline
ζ = 0.01
end = 200
m = 100
k = 100
u0 = 1
u1 = 2
ω = k / m
u = lambda t: np.exp(-ζ * ω * t) * (
    np.cos(t * ω * np.sqrt(1 - ζ**2))
    + (u1 + ζ * ω * u0) / (ω * np.sqrt(1 - ζ**2)) * (np.sin(t * ω * np.sqrt(1 - ζ**2))))
y = np.array([])
for i in range(0,end):
    y = np.append(y,u(i))
x_l = [i for i in range(0,end)]
x = np.array(x_l)

B_spline_coeff = make_interp_spline(x, y)
X_Final = np.linspace(x.min(), x.max(), 500)
Y_Final = B_spline_coeff(X_Final)
plt.plot(X_Final,Y_Final,color="red")
plt.axhline(y=0, color='k', linestyle='-')
plt.grid(True)
plt.show()
